# Technical Account Manager
## Requirements

- 7 + years of experience in a related function is required with direct customer advocacy and engagement experience in post-sales or professional services functions

      **Self Service gitlab CE for VPLEX BU in 2014**

- Prior experience in Customer Success or equivalent history of increasing satisfaction, adoption, and retention

      **Gitlab EE from proposal to importing all gitswarm to production for tool automation in 2016**

- Familiarity working with clients of all sizes, especially large enterprise organizations

      **Apple/Dell EMC to Tableau and Coverity before Synopsys - Mitrend is small**

- Exception verbal, written, organizational, presentation, and communications skills

      **Presentation of my Gitlab history in Boston 2017**

- Detailed oriented and analytic

      **Please fix script for script compare with integer value**

      **Are all gitlab folks safe from #KrackAttach?**

```
 diff --git a/scripts/lint-doc.sh b/scripts/lint-doc.sh
 index e5242fee32..64067e71bc 100755
 --- a/scripts/lint-doc.sh
 +++ b/scripts/lint-doc.sh
 @@ -5,7 +5,7 @@ cd "$(dirname "$0")/.."
  # Use long options (e.g. --header instead of -H) for curl examples in documentation.
  echo 'Checking for curl short options...'
  grep --extended-regexp --recursive --color=auto 'curl (.+ )?-[^- ].*' doc/ >/dev/null 2>&1
 -if [ $? == 0 ]
 +if [ $? -eq 0 ]
```

- Strong team player but self starter

    **My Scrum Team was a pleasure | I pushed regular Gitlab CE/EE deployments**

- Strong technical, analytic and problem solving skills

    **Develop management reporting infrastructure using Tableau for disparate data**

- Experience with Ruby on Rails applications and Git

    **Rubocop, gem install and Rake are my friends**

- Deep knowledge of software development lifecycle and development pipeline

    **Sunset Invista to moving from hudson to Jenkins in 2010**

- Understanding of continuous integration, continuous deployment, chatOps, and cloud native

    **moved from home grown builds to Jenkins/Gitlab CI with xmpp for chatOps - all native cloud (VMware)**

- Above average knowledge of Unix and Unix based Operating Systems

    **Linux Fedora/Ubuntu/RedHat/CentOS/SUSE/OpenSUSE/Kali/ClearOS/Alpine are my friends**

- Installation and operation of Linux operating systems and hardware investigation/manipulation commands

    **Redesigned EMC software portal RedHat deployments to Satellite Server**

- BASH/Shell scripting including systems and init.d startup scripts

    **My gitlab init script friends**

```
sudo gitlab-ctl start
sudo gitlab-ctl status
```

- Package management (RPM, etc. to add/remove/list packages)

    **get ready for apt or dnf along with rpm -i/-e and dpkg -l**

- Understanding of system log files/logging infrastructure

    **We need more gitlab-ctl tail**

- B.Sc. in Computer Science or equivalent experience

    **BS and MS Engineer from WPI**

- Programming/scripting experience & skill is required (Bash & Ruby)

    **Many years of Bash script - Learning Ruby now**

- Project management experience & skills

    **Rebuild scrum team from 2014-->2017**

- SCM admin and/or PS experience would be a plus

    **Admin history includes RCS, ClearCase, SVN, git, gitolite, github and most importantly gitlab CE/EE**

- Set up HA/DR, working with Containers and Schedulers (Kubernetes preferred) and also experience with AWS stack (EC2, ECS, RDS, ElastiCache)

    **over a decade of VMware cloud experience**
