# Question 1
If you are going to install something on Linux, what are the different ways that could be done?

Ubuntu has apt, aptitude, apt-get
RedHat has yum
SuSE has zypper

also, zypper, rpm and dpkg work just fine from CLI

both Ubuntu and RedHat can add repository from packageckoud
https://packagecloud.io/

My current laptop has CE install from curl command below:

```
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash

```


# Question 2
What are the advantages and disadvantages of using a Web framework such as Rails?

Advantage for Gitlab is the product is the #1 Ruby open source project. Ruby on
Rails is feature rich Web Framework.

Disadvantage includes 2017 ruby is not popular.
Most popular frameworks are Sprint MVC, JSF, Struts

Reference:
https://en.wikipedia.org/wiki/Comparison_of_web_frameworks

# Question 3
What would you do if you found a bug in your software and wanted to use git to help identify when this problem started?

bisect the history finding when the code stopped working.

write a regression test after reverting or fixing the bug

# Question 4
Describe the resources or steps you may use to resolve a customer’s problem when you don’t know the answer. (Make assumptions wherever you need to about how the GitLab team works).

1) Can the customer reproduce the bug?

2) Can the customer provide the logs around the time of the bug/issue?

3) Can the Gitlab team reproduce the customer bug/issue?

4) Are all the details of the version and infrastructure well understood?

5) Has the Knowledge base been searched?

6) Can the customer demonstrate live in a Hangout/Skype/WebEX/Meetup with all details?

# Question 5
If you have them, please provide links to what you consider some of your best answers on forums such as Stack Overflow, Reddit, or in projects on GitLab, GitHub, etc.

1) For anything gitlab start with git lab

https://gitlab.com/gitlab-org/gitlab-ce/issues

https://gitlab.com/gitlab-org/gitlab-ee/issues

2) For anything git consider github after git lab

https://github.com/explore

3) Stack Overflow is often an easy source often known issues with  valid solutions

https://stackoverflow.com/questions/tagged/ruby

4) reddit for say reddit/r/ruby always has something to learn on all things ruby

https://www.reddit.com/r/rails/


5) Consider writing to the maintainer of the repository explaining the issue


6) IRC

https://freenode.net/


7) wiki

https://en.wikipedia.org/wiki/Comparison_of_web_frameworks


8) youtube

https://www.youtube.com
